# multiqc Singularity container
### Bionformatics package multiqc<br>
Aggregate results from bioinformatics analyses across many samples into a single report. MultiQC searches a given directory for analysis logs and compiles a HTML report. It's a general use tool, perfect for summarising the output from numerous bioinformatics tools.<br>
multiqc Version: 1.9<br>
[https://github.com/ewels/MultiQC]

Singularity container based on the recipe: Singularity.multiqc_v1.9

Package installation using Miniconda3-4.7.12<br>

Image singularity (V>=3.3) is automatically build and deployed (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

### build:
`sudo singularity build multiqc_v1.9.sif Singularity.multiqc_v1.9`

### Get image help
`singularity run-help ./multiqc_v1.9.sif`

#### Default runscript: STAR
#### Usage:
  `multiqc_v1.9.sif --help`<br>
    or:<br>
  `singularity exec multiqc_v1.9.sif multiqc --help`<br>


### Get image (singularity version >=3.3) with ORAS:<br>
`singularity pull multiqc_v1.9.sif oras://registry.forgemia.inra.fr/gafl/singularity/multiqc/multiqc:latest`


